/**
 * [description]管理初始化
 */
var [entityClass] = {
    id: "[entityClass]Table",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
[entityClass].initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '标题', field: 'title', align: 'center', valign: 'middle'},
        {title: '内容', field: 'content', align: 'center', valign: 'middle'},
        {title: '发布者', field: 'createrName', align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createtime', align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
[entityClass].check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        [entityClass].seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加[description]
 */
[entityClass].openAdd[entityClass] = function () {
    var index = layer.open({
        type: 2,
        title: '添加[description]',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/[lowerentity]/[lowerentity]_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看[description]详情
 */
[entityClass].open[entityClass]Detail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '[description]详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/[lowerentity]/[lowerentity]_update/' + [entityClass].seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除[description]
 */
[entityClass].delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/[lowerentity]/delete", function (data) {
            Feng.success("删除成功!");
            [entityClass].table.refresh();
        }, function (data) {
            Feng.error("删除失败!");
        });
        ajax.set("id", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询[description]列表
 */
[entityClass].search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    [entityClass].table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = [entityClass].initColumn();
    var table = new BSTable([entityClass].id, "/[lowerentity]/list", defaultColunms);
    table.setPaginationType("client");
    [entityClass].table = table.init();
});
