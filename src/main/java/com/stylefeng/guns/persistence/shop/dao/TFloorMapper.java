package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.persistence.shop.model.TFloor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 商品类型表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TFloorMapper extends BaseMapper<TFloor> {

}