package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.persistence.shop.model.TMember;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TMemberMapper extends BaseMapper<TMember> {

}