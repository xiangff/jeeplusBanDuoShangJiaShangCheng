package com.stylefeng.guns.persistence.test.dao;

import com.stylefeng.guns.persistence.test.model.TCard;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2017-05-22
 */
public interface TCardMapper extends BaseMapper<TCard> {

}