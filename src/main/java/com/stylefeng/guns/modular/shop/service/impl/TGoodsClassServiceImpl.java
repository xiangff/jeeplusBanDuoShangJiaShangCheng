package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.common.node.ZTreeNode;
import com.stylefeng.guns.persistence.shop.model.TGoodsClass;
import com.stylefeng.guns.persistence.shop.dao.TGoodsClassMapper;
import com.stylefeng.guns.modular.shop.service.ITGoodsClassService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TGoodsClassServiceImpl extends ServiceImpl<TGoodsClassMapper, TGoodsClass> implements ITGoodsClassService {

    @Resource
    TGoodsClassMapper tGoodsClassMapper;
    @Override
    public List<ZTreeNode> tree() {
        return tGoodsClassMapper.tree();
    }
}
