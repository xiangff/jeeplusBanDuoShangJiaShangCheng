package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TReply;
import com.stylefeng.guns.persistence.shop.dao.TReplyMapper;
import com.stylefeng.guns.modular.shop.service.ITReplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TReplyServiceImpl extends ServiceImpl<TReplyMapper, TReply> implements ITReplyService {
	
}
