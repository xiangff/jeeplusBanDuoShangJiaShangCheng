package com.stylefeng.guns.modular.shop.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.persistence.shop.model.TBrand;
import com.stylefeng.guns.persistence.shop.model.TFloor;

/**
 * <p>
 * 商品类型表 服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITBrandService extends IService<TBrand> {
	
}
